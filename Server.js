const carbone = require('carbone');
const express = require('express');
const fs = require ('fs')
const shortid = require('shortid');
const fileUpload = require('express-fileupload');
const app = express();
const port = process.env.PORT || 3000;


app.listen(port, () => console.log(`Reporting Tool is listening on port ${port}!`));
app.use(express.json({limit: '50mb'}));
app.use(fileUpload);

app.get('/', (req, res) => {res.send("Alive")})

app.post('/', async function (req, res)
{
    //check if files where uploaded
    if (!req.files.FileData || Object.keys(req.files.FileData).length === 0)
    {
        return res.status(400).send('no json');
    }
    if (!req.files.FileTemplate || Object.keys(req.files.FileTemplate).length === 0)
    {
        return res.status(400).send('no template');
    }

    //getting buffer and converting it to JSON --> data
    const dataBuffer = Buffer.from(req.files.FileData.data);
    const buf = dataBuffer.toString('utf-8')
    data = JSON.parse(buf);

    //Template --> path
    template = getTemplatePath(Buffer.from(req.files.FileTemplate.data));

    //Carbone function (template + data--> Creates the Report)
    carbone.render(template, data, function(err, result)
    {
        if (err)
        {
            return console.log(err);
        }

        //data after the carbone conversion
        res.send(result)

        //deleting the used Template
        fs.unlinkSync(template)
    });
})

function getTemplatePath(templateData)
{
    Name = shortid.generate()+'.xlsx';
    fs.writeFileSync(__dirname+'/tmp/'+Name, templateData);
    path = __dirname+'/tmp/'+Name;
    return path
}