FROM node:17

WORKDIR /carbone-server

COPY ["package.json", "package-lock.json*", "./"]

RUN npm install

COPY . .

EXPOSE 3000

CMD ["node","Server.js"]
